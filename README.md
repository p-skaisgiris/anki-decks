# anki-decks

University course and other [Anki](https://apps.ankiweb.net/) flashcard decks. To learn or to re-learn/brush-up.

**Use at your own risk! Not guaranteed to be fully correct.**

## Reinforcement Learning
University of Amsterdam, Fall 2023

The flashcards were based primarily on the slides by Herke van Hoof used in the course. Our main reference for the course was the book Reinforcement Learning by Sutton and Barto. Some concepts were complented using the following resources:
- [RL cheatsheets by Shervine Amidi and Afshine Amidi](https://stanford.edu/~shervine/teaching/cs-221/cheatsheet-states-models#markov-decision-processes)
- [Berkeley RL](http://rail.eecs.berkeley.edu/deeprlcourse/)
- [Open AI Spinning Up](https://spinningup.openai.com/en/latest/spinningup/rl_intro.html)

### Topics covered
- Intro to RL
- Dynamic Programming
- Monte Carlo (on and off policy)
- Temporal Difference, SARSA, Q-Learning
- Limitations of tabular methods, Gradient MC
- Semi-gradient methods, tile coding, linear value approximation
- Linear value function geometry, Deep Q Networks
- Limitations of action-value methods, policy gradient methods (MC, REINFORCE)
- Actor-critic methods, (deep) deterministic policy gradients, practical RL
- Natural Policy Gradients, Trust Region Policy Optimization

## Causality
University of Amsterdam, Fall 2023.

The flashcards were based primarily on the slides by Sara Magliacane used in the course. One of the main references throughout the course was the book Elements of Causal Inference by Peters, Janzing and Schölkopf.

### Topics covered
- Pearl's causal hierarchy
- Graph terminology, causal graphs, bayesian networks
- D-separation
- The do operator, structural causal models
- Adjustment sets, backdoor, frontdoor, adjustment criterions, instrumental variables
- Causal counterfactuals, estimating causal effects
- Causal graph discovery
- Markov equivalence, constraint-based causal discovery
- Score-based causal discovery, restricted models
- Do-calculus, transportability
- Invariant causal prediction, causality-inspired machine learning

## Information Theory
University of Amsterdam, Fall 2023.

The flashcards are primarily based on the slides by Nicolas Resch and course notes by Yfke Dulek and Christian Schaffner.

### Topics covered
- Shannon entropy
- Mutual information, relative entropy
- Entropy diagrams
- Symbol, Huffman, Arithmetic codes
- Kraft's inequality
- Shannon's source coding theorem
- Asymptotic Equipartition Property
- Perfectly secure encryption
- Finite Markov Chains
- Stochastic processes
- Entropy rate
- Channels
- Code in Channels
- Confusability graphs
- Shannon, channel capacities
- Achievable capacity rate
- Shannon's noisy-channel coding theorem
- Source-Channel separation

## Machine Learning Theory
Mastermath, University of Amsterdam, Spring 2023.

The flashcards were primarily based on Understanding Machine Learning: From Theory to Algorithms by Shai Shalev-Shwartz and Shai Ben-David. I also drew inspiration from [the slides from this course](https://homepages.cwi.nl/~wmkoolen/MLT_2024/) by Wouter M. Koolen and Tim van Erven.

### Topics covered
- Empirical risk minimization
- Sample complexity
- PAC learning with and without realizability assumptions
- Uniform convergence
- No-free-lunch theorem
- Shattering, VC dimension
- The fundamental theorem of statistical learning, growth function, Sauer's lemma
- Union bound, Hoeffding's, Jensen's, McDiarmid's inequalities
- Rademacher complexity
- Rademacher calculus, properties
- Non-uniform learnability

---
